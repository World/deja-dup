/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

void metadir_ignored()
{
  var tree = new DejaDup.FileTree();

  tree.add("random/file", FileType.REGULAR);

  // Current location
  tree.add(
    Path.build_filename(
      Environment.get_user_data_dir(),
      Config.PACKAGE,
      "metadata",
      "README"
    ),
    FileType.REGULAR
  );

  // Old location
  tree.add(
    Path.build_filename(
      Environment.get_user_cache_dir(),
      Config.PACKAGE,
      "metadata",
      "README"
    ),
    FileType.REGULAR
  );

  // Confirm we don't remove dirs with more content than we expect
  tree.add(
    Path.build_filename("multiple", Config.PACKAGE, "metadata", "README"),
    FileType.REGULAR
  );
  tree.add(
    Path.build_filename("multiple", Config.PACKAGE, "metadata", "hello.txt"),
    FileType.REGULAR
  );

  // Confirm that we start with 3 roots - /random, /multiple, and /home-root
  assert(tree.root.children.size() == 3);

  tree.finish();

  // OK confirm the tree filters out the home entry
  assert(tree.root.children.size() == 2);
  assert(tree.root.children.contains("multiple"));
  assert(tree.root.children.contains("random"));
}

void parse_one_dir (string to_parse, string? result)
{
  if (result != null)
    assert(DejaDup.parse_dir(to_parse).equal(File.new_for_path(result)));
}

void parse_dir()
{
  parse_one_dir("", Environment.get_home_dir());
  parse_one_dir("$HOME", Environment.get_home_dir());
  parse_one_dir("$TRASH", DejaDup.InstallEnv.instance().get_trash_dir());
  parse_one_dir("$DESKTOP", Environment.get_user_special_dir(UserDirectory.DESKTOP));
  parse_one_dir("$DOCUMENTS", Environment.get_user_special_dir(UserDirectory.DOCUMENTS));
  parse_one_dir("$DOWNLOAD", Environment.get_user_special_dir(UserDirectory.DOWNLOAD));
  parse_one_dir("$MUSIC", Environment.get_user_special_dir(UserDirectory.MUSIC));
  parse_one_dir("$PICTURES", Environment.get_user_special_dir(UserDirectory.PICTURES));
  parse_one_dir("$PUBLIC_SHARE", Environment.get_user_special_dir(UserDirectory.PUBLIC_SHARE));
  parse_one_dir("$TEMPLATES", Environment.get_user_special_dir(UserDirectory.TEMPLATES));
  parse_one_dir("/backup/$USER", Path.build_filename("/backup", Environment.get_user_name()));
  parse_one_dir("backup/$USER", Path.build_filename(Environment.get_home_dir(), "backup", Environment.get_user_name()));
  parse_one_dir("$VIDEOS", Environment.get_user_special_dir(UserDirectory.VIDEOS));
  parse_one_dir("VIDEOS", Path.build_filename(Environment.get_home_dir(), "VIDEOS"));
  parse_one_dir("/VIDEOS", "/VIDEOS");
  parse_one_dir("file:///VIDEOS", "/VIDEOS");
  assert(DejaDup.parse_dir("file:VIDEOS").equal(File.parse_name("file:VIDEOS")));
}

void parse_one_version(string str, int maj, int min, int mic)
{
  int pmaj, pmin, pmic;
  assert(DejaDup.parse_version(str, out pmaj, out pmin, out pmic));
  assert(pmaj == maj);
  assert(pmin == min);
  assert(pmic == mic);
}

void parse_bad_version(string str)
{
  int pmaj, pmin, pmic;
  assert(!DejaDup.parse_version(str, out pmaj, out pmin, out pmic));
  assert(pmaj == 0);
  assert(pmin == 0);
  assert(pmic == 0);
}

void parse_version()
{
  parse_bad_version("");
  parse_one_version("a", 0, 0, 0);
  parse_one_version("1", 1, 0, 0);
  parse_one_version("1.2", 1, 2, 0);
  parse_one_version("1.2.3", 1, 2, 3);
  parse_one_version("1.2.3.4", 1, 2, 3);
  parse_one_version("1.2.3a4", 1, 2, 3);
  parse_one_version("1.2a3.4", 1, 2, 4);
  parse_one_version("1.2 3.4", 1, 2, 4);
  parse_one_version("1.2-3.4", 1, 2, 4);
}

void prompt()
{
  var settings = DejaDup.get_settings();

  settings.set_string(DejaDup.PROMPT_CHECK_KEY, "");
  DejaDup.update_prompt_time(true);
  assert(settings.get_string(DejaDup.PROMPT_CHECK_KEY) == "disabled");

  assert(DejaDup.make_prompt_check() == false);
  assert(settings.get_string(DejaDup.PROMPT_CHECK_KEY) == "disabled");
  DejaDup.update_prompt_time(); // shouldn't change anything
  assert(settings.get_string(DejaDup.PROMPT_CHECK_KEY) == "disabled");

  settings.set_string(DejaDup.PROMPT_CHECK_KEY, "");
  assert(DejaDup.make_prompt_check() == false);
  var time_now = settings.get_string(DejaDup.PROMPT_CHECK_KEY);
  assert(time_now != "");
  assert(DejaDup.make_prompt_check() == false);
  assert(settings.get_string(DejaDup.PROMPT_CHECK_KEY) == time_now);

  var cur_time = new DateTime.now_local();
  cur_time = cur_time.add_seconds(-1 * DejaDup.get_prompt_delay());
  cur_time = cur_time.add_hours(1);
  settings.set_string(DejaDup.PROMPT_CHECK_KEY, cur_time.format("%Y-%m-%dT%H:%M:%S%z"));
  assert(DejaDup.make_prompt_check() == false);

  cur_time = cur_time.add_hours(-2);
  settings.set_string(DejaDup.PROMPT_CHECK_KEY, cur_time.format("%Y-%m-%dT%H:%M:%S%z"));
  assert(DejaDup.make_prompt_check() == true);
}

void tool_migration()
{
  DejaDup.get_settings().set_string("tool", "restic");
  DejaDup.get_settings("Google").set_string("folder", "/root/");
  DejaDup.get_settings("Local").set_string("folder", "");

  DejaDup.initialize();

  assert(DejaDup.get_settings().get_string("tool") == "migrated");
  assert(DejaDup.get_settings().get_string("tool-when-new") == "restic");
  assert(DejaDup.get_settings("Drive").get_string("folder") == "$HOSTNAME/restic");
  assert(DejaDup.get_settings("Google").get_string("folder") == "/root/restic");
  assert(DejaDup.get_settings("Local").get_string("folder") == "restic");
  assert(DejaDup.get_settings("Microsoft").get_string("folder") == "$HOSTNAME/restic");
  assert(DejaDup.get_settings("Remote").get_string("folder") == "$HOSTNAME/restic");
}

async void tool_detect() throws Error
{
  var root = DirUtils.make_tmp("deja-XXXXXX");
  DejaDup.get_settings("Local").set_string("folder", root);

  var backend = new DejaDup.BackendLocal(null);
  yield backend.prepare();

  DejaDup.ToolPlugin plugin;

  // check that the default tool on an empty folder is correct
  plugin = yield DejaDup.get_tool_for_backend(backend);
#if ENABLE_RESTIC && RESTIC_BY_DEFAULT
  assert(plugin.name == "restic");
#else
  assert(plugin.name == "duplicity");
#endif

  // can adjust the tool for empty folders
  DejaDup.get_settings().set_string("tool-when-new", "restic");
  plugin = yield DejaDup.get_tool_for_backend(backend);
  assert(plugin.name == "restic");

  DejaDup.get_settings().set_string("tool-when-new", "duplicity");
  plugin = yield DejaDup.get_tool_for_backend(backend);
  assert(plugin.name == "duplicity");

  // can detect duplicity files
  DejaDup.get_settings("Local").set_string("folder", root + "/dup");
  yield backend.prepare();
  FileUtils.set_contents(root + "/dup/duplicity-longname.vol1.gpg", "");
  DirUtils.create(root + "/dup/restic", 0700); // confirm this doesn't confuse us
  plugin = yield DejaDup.get_tool_for_backend(backend);
  assert(plugin.name == "duplicity");

  // can detect restic files
  DejaDup.get_settings("Local").set_string("folder", root + "/rest");
  yield backend.prepare();
  FileUtils.set_contents(root + "/rest/config", "");
  DirUtils.create(root + "/rest/data", 0700);
  DirUtils.create(root + "/rest/index", 0700);
  DirUtils.create(root + "/rest/keys", 0700);
  DirUtils.create(root + "/rest/locks", 0700);
  DirUtils.create(root + "/rest/snapshots", 0700);
  plugin = yield DejaDup.get_tool_for_backend(backend);
  assert(plugin.name == "restic");

  // errors out with unknown files
  DejaDup.get_settings("Local").set_string("folder", root + "/bogus");
  yield backend.prepare();
  FileUtils.set_contents(root + "/bogus/hello.txt", "");
  try {
    yield DejaDup.get_tool_for_backend(backend);
    assert(false);
  } catch (OptionError.FAILED err) {
    assert(err.message == "Unrecognized files in storage location. Choose an empty folder instead.");
  }
}
void wrap_tool_detect()
{
  var loop = new MainLoop();
  tool_detect.begin((obj, res) => {
    try {
      tool_detect.end(res);
    } catch (Error err) {
      warning("%s", err.message);
      assert(false);
    }
    loop.quit();
  });
  loop.run();
}

string get_srcdir()
{
  var srcdir = Environment.get_variable("srcdir");
  if (srcdir == null)
    srcdir = ".";
  return srcdir;
}

void setup()
{
}

void reset_keys(Settings settings)
{
  var source = SettingsSchemaSource.get_default();
  var schema = source.lookup(settings.schema_id, true);

  foreach (string key in schema.list_keys())
    settings.reset(key);

  foreach (string child in schema.list_children())
    reset_keys(settings.get_child(child));
}

void teardown()
{
  reset_keys(new Settings(Config.APPLICATION_ID));
}

int main(string[] args)
{
  Test.init(ref args);

  Environment.set_variable("PATH",
                           get_srcdir() + "/../mock:" +
                             Environment.get_variable("PATH"),
                           true);

  var unit = new TestSuite("libdeja");
  unit.add(new TestCase("metadir-ignored", setup, metadir_ignored, teardown));
  unit.add(new TestCase("parse-dir", setup, parse_dir, teardown));
  unit.add(new TestCase("parse-version", setup, parse_version, teardown));
  unit.add(new TestCase("prompt", setup, prompt, teardown));
  unit.add(new TestCase("tool-migration", setup, tool_migration, teardown));
  unit.add(new TestCase("tool-detect", setup, wrap_tool_detect, teardown));
  TestSuite.get_root().add_suite((owned)unit);

  return Test.run();
}
