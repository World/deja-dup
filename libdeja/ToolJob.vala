/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

// A "snapshot" is a collection of files and data at a point in time.
// Duplicity calls them "backup sets", Borg calls them "archives",
// Restic calls them "snapshots".
public class DejaDup.SnapshotInfo : Object
{
  // Time associated with the snapshot by the tool (start or end :shrug:)
  public DateTime time;

  // A tag is a machine-readable identifier for a specific snapshot
  public string tag;

  // An "external" snapshot is one that isn't from Deja Dup on this machine.
  // An external description is a human-readable label to help differentiate
  // this snapshot (could contain extra info like what hostname was used)
  // from the user's normal snapshots.
  // Should be short (likely shown in a date dropdown).
  public string external_description;

  public SnapshotInfo(DateTime time, string tag)
  {
    this.time = time;
    this.tag = tag;
  }
}

// Any changes here should be also reflected in ToolJobChain.sync_job
public abstract class DejaDup.ToolJob : Object
{
  // life cycle signals
  public signal void done(bool success, bool cancelled);
  public signal void raise_error(string errstr, string? detail);

  // hints to UI
  public signal void action_desc_changed(string action);
  public signal void action_file_changed(File file, bool actual);
  public signal void progress(double percent);
  public signal void is_full(bool first);
  public signal void local_file_error(string file); // I/o error when reading/writing

  // hints that interaction is needed
  public signal void bad_encryption_password();
  public signal void question(string title, string msg);

  // type-specific signals
  public signal void collection_dates(List<SnapshotInfo> infos); // STATUS
  public signal void listed_current_files(string file, FileType type); // LIST, as full file path (/home/me/file.txt)

  // life cycle control
  public abstract async void start ();
  public abstract void cancel (); // destroy progress so far
  public abstract void stop (); // just abruptly stop
  public abstract void pause (string? reason);
  public abstract void resume ();

  public enum Mode {
    INVALID,
    BACKUP,
    RESTORE,
    LIST_SNAPSHOTS,
    LIST_FILES,
    VERIFY_BASIC, // simple verify, done after every backup
    VERIFY_CLEAN, // the 'nag' verify (no cache, no remembered password)
    LAST_MODE,
  }
  public Mode mode {get; set; default = Mode.INVALID;}

  public enum Flags {
    NO_PROGRESS,
  }
  public Flags flags {get; set;}

  public File local {get; set;}
  public Backend backend {get; set;}
  public string encrypt_password {get; set;}
  public string tag {get; set;}

  public List<File> includes; // BACKUP
  public List<File> excludes; // BACKUP
  public List<string> exclude_regexps; // BACKUP

  protected List<File> _restore_files;
  public List<File> restore_files { // RESTORE
    get {
      return this._restore_files;
    }
    set {
      this._restore_files = value.copy_deep ((CopyFunc) Object.ref);
    }
  }
  public FileTree tree {get; set;} // RESTORE
}
