/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

/* This is meant to be used right after a successful OperationBackup to
   verify the results. */

public class DejaDup.OperationVerify : Operation
{
  public string tag {get; construct;}

  public OperationVerify(Backend backend, string tag) {
    var mode = is_nag_time() ? ToolJob.Mode.VERIFY_CLEAN : ToolJob.Mode.VERIFY_BASIC;
    Object(mode: mode, backend: backend, tag: tag);
  }

  construct {
    // Should we nag user about password, etc?  What this really means is that
    // we try to do our normal verification routine in as close an emulation
    // to a fresh restore after a disaster as possible.  So fresh cache, no
    // saved password, etc.  We do *not* explicitly unmount the backend,
    // because we may not be the only consumers.
    if (mode == ToolJob.Mode.VERIFY_CLEAN) {
      use_cached_password = false;
    }
  }

  public async override void start()
  {
    if (mode == ToolJob.Mode.VERIFY_CLEAN) {
      var fake_state = new State();
      fake_state.backend = backend;//.ref() as DejaDup.Backend;
      set_state(fake_state);
    }
    action_desc_changed(_("Verifying backup…"));
    yield base.start();
  }

  protected override void connect_to_job()
  {
    job.tag = tag;
    base.connect_to_job();
  }

  internal async override void operation_finished(bool success, bool cancelled)
  {
    if (success && mode == ToolJob.Mode.VERIFY_CLEAN) {
      update_nag_time();
    }

    yield base.operation_finished(success, cancelled);
  }
}
