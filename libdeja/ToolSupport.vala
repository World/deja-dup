/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

// This file holds some abstractions around loading which ToolPlugin to use.
// (i.e. whether to use Restic or Duplicity)

using GLib;

namespace DejaDup {

string unsupported_msg(string tool_name)
{
  // Translators: '%s' is a backup tool name like Restic or Borg
  return _("%s backups are not supported in this release. Try using another storage location.").printf(tool_name);
}

#if ENABLE_BORG
ToolPlugin borg_tool = null;
#endif
ToolPlugin make_borg_tool() throws OptionError
{
#if ENABLE_BORG
  if (borg_tool == null)
    borg_tool = new BorgPlugin();
  return borg_tool;
#else
  throw new OptionError.BAD_VALUE(unsupported_msg("Borg"));
#endif
}

#if ENABLE_RESTIC
ToolPlugin restic_tool = null;
#endif
ToolPlugin make_restic_tool() throws OptionError
{
#if ENABLE_RESTIC
  if (restic_tool == null)
    restic_tool = new ResticPlugin();
  return restic_tool;
#else
  throw new OptionError.BAD_VALUE(unsupported_msg("Restic"));
#endif
}

ToolPlugin duplicity_tool = null;
ToolPlugin make_duplicity_tool()
{
  if (duplicity_tool == null)
    duplicity_tool = new DuplicityPlugin();
  return duplicity_tool;
}

ToolPlugin make_tool(string tool_name) throws OptionError
{
  switch(tool_name)
  {
    case "borg":
      return make_borg_tool();
    case "restic":
      return make_restic_tool();
    case "auto":
#if ENABLE_RESTIC && RESTIC_BY_DEFAULT
      return make_restic_tool();
#else
      return make_duplicity_tool();
#endif
    default:
      return make_duplicity_tool();
  }
}

public ToolPlugin get_default_tool()
{
  var settings = get_settings();
  var tool_name = settings.get_string(TOOL_WHEN_NEW_KEY);
  try {
    return make_tool(tool_name);
  } catch (OptionError err) {
    return make_duplicity_tool();
  }
}

// Backend must already have prepare() called. This will inspect the files
// in the backend location to see which tool is being used there.
public async ToolPlugin get_tool_for_backend(Backend backend) throws OptionError
{
  var filenames = yield backend.peek_at_files();
  filenames.sort(strcmp);

  // If the folder is empty, let's use the default tool
  if (filenames == null)
    return get_default_tool();

  // All duplicity files have the same basic prefix. We also must allow for a
  // legacy "restic" folder, which works here because the filenames are sorted
  // and duplicity- ones will come first.
  if (filenames.data.has_prefix("duplicity-"))
    return make_tool("duplicity");

  // Restic repos have these entries:
  //   config data/ index/ keys/ snapshots/ (and sometimes locks/)
  // Let's check for those, but allow other files, to try to be a little
  // future proof in case Restic expands its file format on us.
  string[] expected = {"config", "data", "index", "keys", "snapshots"};
  foreach (var filename in filenames) {
    for (int i = 0; i < expected.length; i++) {
      if (filename == expected[i]) {
        expected[i] = null;
        break;
      }
    }
  }
  var found_all = true;
  for (int i = 0; i < expected.length; i++) {
    if (expected[i] != null) {
      found_all = false;
      break;
    }
  }
  if (found_all)
    return make_tool("restic");

  // Error case (unrecognized files)
  throw new OptionError.FAILED(_("Unrecognized files in storage location. Choose an empty folder instead."));
}

}
