/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

internal class Rclone : Object
{
  static string fill_envp_from_google(DejaDup.BackendGoogle google_backend, ref List<string> envp)
  {
    envp.append("RCLONE_CONFIG_DEJADUPDRIVE_TYPE=drive");
    envp.append("RCLONE_CONFIG_DEJADUPDRIVE_CLIENT_ID=" + Config.GOOGLE_CLIENT_ID);
    envp.append("RCLONE_CONFIG_DEJADUPDRIVE_TOKEN=" + google_backend.full_token);
    envp.append("RCLONE_CONFIG_DEJADUPDRIVE_SCOPE=drive.file");
    envp.append("RCLONE_CONFIG_DEJADUPDRIVE_USE_TRASH=false");
    return "dejadupdrive:" + google_backend.get_folder();
  }

  static string fill_envp_from_microsoft(DejaDup.BackendMicrosoft microsoft_backend, ref List<string> envp)
  {
    envp.append("RCLONE_CONFIG_DEJADUPONEDRIVE_TYPE=onedrive");
    envp.append("RCLONE_CONFIG_DEJADUPONEDRIVE_CLIENT_ID=" + Config.MICROSOFT_CLIENT_ID);
    envp.append("RCLONE_CONFIG_DEJADUPONEDRIVE_TOKEN=" + microsoft_backend.full_token);
    envp.append("RCLONE_CONFIG_DEJADUPONEDRIVE_DRIVE_ID=" + microsoft_backend.drive_id);
    envp.append("RCLONE_CONFIG_DEJADUPONEDRIVE_DRIVE_TYPE=personal");
    return "dejaduponedrive:" + microsoft_backend.get_folder();
  }

  public static string? fill_envp_from_backend(DejaDup.Backend backend, ref List<string> envp)
  {
    var google_backend = backend as DejaDup.BackendGoogle;
    if (google_backend != null)
      return fill_envp_from_google(google_backend, ref envp);

    var microsoft_backend = backend as DejaDup.BackendMicrosoft;
    if (microsoft_backend != null)
      return fill_envp_from_microsoft(microsoft_backend, ref envp);

    return null;
  }

  public static string rclone_command()
  {
    var testing_str = Environment.get_variable("DEJA_DUP_TESTING");
    if (testing_str != null && int.parse(testing_str) > 0)
      return "rclone";
    else
      return Config.RCLONE_COMMAND;
  }

  public static async List<string> list_files(DejaDup.Backend backend, int max)
  {
    var envp = new List<string>();
    var target = Rclone.fill_envp_from_backend(backend, ref envp);

    var launcher = new SubprocessLauncher(
      SubprocessFlags.STDOUT_PIPE | SubprocessFlags.STDERR_SILENCE
    );

    // Set environment
    foreach (var env in envp) {
      var parts = env.split("=", 2);
      launcher.setenv(parts[0], parts[1], true);
    }

    // Launch it!
    Subprocess rclone;
    try {
      rclone = launcher.spawn(Rclone.rclone_command(), "lsf", target);
    } catch (Error err) {
      warning("Could not launch Rclone to list files: %s", err.message);
      return new List<string>();
    }

    // Grab stdout
    var stdout_raw = rclone.get_stdout_pipe();
    var stdout = new DataInputStream(stdout_raw);

    // And gather up the first `max` lines
    var files = new List<string>();
    for (int i = 0; i < max; i++) {
      try {
        var line = yield stdout.read_line_utf8_async(Priority.LOW);
        if (line == null)
          break;
        if (line.data[line.length - 1] == '/')
          line.data[line.length - 1] = 0;
        files.append(line);
      } catch (Error err) {
        warning("Could not parse Rclone output: %s", err.message);
        break;
      }
    }

    return files;
  }
}
