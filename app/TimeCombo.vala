/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/TimeCombo.ui")]
public class TimeCombo : Adw.Bin
{
  public string when { get; private set; default = null; }

  public void register_operation(DejaDup.OperationStatus op)
  {
    clear();
    op.collection_dates.connect(fill_combo_with_dates);
  }

  public string get_active_text()
  {
    var item = (Item)combo.selected_item;
    return item == null ? null : item.label;
  }

  public void clear()
  {
    store.remove_all();
    when = null;
    box.visible = false;
  }

  [GtkChild]
  unowned Gtk.Box box;
  [GtkChild]
  unowned Gtk.DropDown combo;

  ListStore store;
  construct
  {
    store = new ListStore(typeof(Item));
    combo.model = store;
    combo.notify["selected-item"].connect(update_when);
  }

  public class Item : Object {
    public string label {get; internal set;}
    public string tag {get; construct;}

    public Item(string label, string tag) {
      Object(label: label, tag: tag);
    }
  }

  void update_when() {
    var item = (Item)combo.selected_item;
    when = item == null ? null : item.tag;
  }

  static bool is_same_day(DateTime one, DateTime two)
  {
    return one.get_year() == two.get_year() &&
           one.get_day_of_year() == two.get_day_of_year();
  }

  string label_for_info(DejaDup.SnapshotInfo info, string format)
  {
    string time = info.time.format(format);
    if (info.external_description != null && info.external_description != "") {
      return time + " (" + info.external_description + ")";
    }
    return time;
  }

  void fill_combo_with_dates(DejaDup.OperationStatus op, List<DejaDup.SnapshotInfo> dates)
  {
    clear();
    if (dates.length() == 0)
      return;

    dates.sort((a, b) => {return a.time.compare(b.time);});

    int first_internal_snapshot = -1;
    DejaDup.SnapshotInfo prev_info = null;
    dates.foreach((info) => {
      info.time = info.time.to_local();

      var format = "%x";
      if (prev_info != null && is_same_day(prev_info.time, info.time)) {
        // Translators: %x is the current date, %X is the current time.
        // This will be in a list with other strings that just have %x (the
        // current date).  So make sure if you change this, it still makes
        // sense in that context.
        format = _("%x %X");

        // Replace previous item's label too (in case it was the first on this day)
        ((Item)store.get_item(0)).label = label_for_info(prev_info, format);
      }

      prev_info = info;
      var user_str = label_for_info(info, format);
      store.insert(0, new Item(user_str, info.tag));

      // Keep track of the most recent snapshot that is internal
      if (info.external_description == null || info.external_description == "")
        first_internal_snapshot = 0;
      else if (first_internal_snapshot >= 0)
        first_internal_snapshot += 1;
    });

    combo.selected = int.max(0, first_internal_snapshot);
    box.visible = true;
  }
}
