/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

// This class exists just to be a buffer between a mount action which might not
// be user-initiated (like for an automatic backup) and a window popping up for
// the user. We don't want sudden windows appearing on the user's desktop.
//
// So instead, we interrupt the assistant and offer a button that then pops up
// the mount password window. Very similar to the oauth consent page.

public class MountOperationAssistant : MountOperation
{
  public bool retry_mode {get; set; default = false;} // skip any questions, send existing data back

  unowned AssistantOperation assist;
  Gtk.Widget button_page;
  Gtk.Label message_label;
  Gtk.Button button;
  Gtk.MountOperation _inner_op;

  enum OperationMode {
    PASSWORD,
    QUESTION,
    PROCESSES,
  }

  OperationMode op_mode;
  string message;
  string default_user;
  string default_domain;
  AskPasswordFlags flags;
  string[] choices;
  Array<Pid> processes;

  public MountOperationAssistant(AssistantOperation assist)
  {
    this.assist = assist;
    assist.backward.connect(do_backward);
    assist.closing.connect(do_close);
    button_page = add_button_page();
  }

  Gtk.MountOperation get_inner_op()
  {
    if (_inner_op == null) {
      _inner_op = new Gtk.MountOperation(this.assist.root as Gtk.Window);
      _inner_op.bind_property("anonymous", this, "anonymous", BindingFlags.DEFAULT);
      _inner_op.bind_property("choice", this, "choice", BindingFlags.DEFAULT);
      _inner_op.bind_property("domain", this, "domain", BindingFlags.DEFAULT);
      _inner_op.bind_property("is-tcrypt-hidden-volume", this, "is-tcrypt-hidden-volume", BindingFlags.DEFAULT);
      _inner_op.bind_property("is-tcrypt-system-volume", this, "is-tcrypt-system-volume", BindingFlags.DEFAULT);
      _inner_op.bind_property("password", this, "password", BindingFlags.DEFAULT);
      _inner_op.bind_property("password-save", this, "password-save", BindingFlags.DEFAULT);
      _inner_op.bind_property("pim", this, "pim", BindingFlags.DEFAULT);
      _inner_op.bind_property("username", this, "username", BindingFlags.DEFAULT);
      _inner_op.reply.connect(got_reply);
    }
    return _inner_op;
  }

  public override void aborted()
  {
    assist.show_error(_("Location not available"), null);
  }

  public override void ask_password(string message, string default_user,
                                    string default_domain, AskPasswordFlags flags)
  {
    if (retry_mode) {
      reply(MountOperationResult.HANDLED);
      return;
    }

    this.op_mode = OperationMode.PASSWORD;
    this.message = message;
    this.default_user = default_user;
    this.default_domain = default_domain;
    this.flags = flags;

    switch_to_page(get_first_line(message));
  }

  public override void ask_question(
    string message,
    [CCode (array_length = false, array_null_terminated = true)] string[] choices
  )
  {
    this.op_mode = OperationMode.QUESTION;
    this.message = message;
    this.choices = strdupv(choices);

    switch_to_page(get_first_line(message));
  }

  public override void show_processes(
    string message,
    Array<Pid> processes,
    [CCode (array_length = false, array_null_terminated = true)] string[] choices
  )
  {
    this.op_mode = OperationMode.PROCESSES;
    this.message = message;
    this.processes = processes.copy();
    this.choices = strdupv(choices);

    switch_to_page(get_first_line(message));
  }

  string get_first_line(string message)
  {
    var tokens = message.split("\n", 2);
    if (tokens.length > 0)
      return tokens[0];
    else
      return "";
  }

  void switch_to_page(string header)
  {
    message_label.label = header;
    assist.interrupt(button_page, false);
    assist.set_default_widget(button);
    Notifications.attention_needed(assist, header);
  }

  void button_clicked()
  {
    if (op_mode == OperationMode.PASSWORD) {
      get_inner_op().ask_password(message, default_user, default_domain, flags);
    } else if (op_mode == OperationMode.QUESTION) {
      get_inner_op().ask_question(message, choices);
    } else if (op_mode == OperationMode.PROCESSES) {
      get_inner_op().show_processes(message, processes, choices);
    } else {
      warning("Unknown mount operation mode '%d'", op_mode);
    }
  }

  Gtk.Widget add_button_page()
  {
    int rows = 0;

    var page = new Gtk.Grid();
    page.row_spacing = 24;
    page.column_spacing = 6;
    page.halign = Gtk.Align.CENTER;
    DejaDup.set_margins(page, 12);

    message_label = new Gtk.Label("");
    message_label.xalign = 0.0f;
    message_label.max_width_chars = 35;
    message_label.wrap = true;
    page.attach(message_label, 0, rows, 3, 1);
    ++rows;

    button = new Gtk.Button.with_mnemonic(_("Co_ntinue"));
    button.add_css_class("suggested-action");
    button.clicked.connect(button_clicked);
    page.attach(button, 1, rows, 1, 1);
    ++rows;

    assist.append_page(page, Assistant.Type.INTERRUPT);
    return page;
  }

  void got_reply(MountOperationResult result)
  {
    if (result == MountOperationResult.HANDLED)
      this.assist.go_forward();
    else
      this.assist.canceled();
    reply(result);
  }

  void do_close()
  {
    reply(MountOperationResult.ABORTED);
  }

  void do_backward()
  {
    reply(MountOperationResult.ABORTED);
  }
}
