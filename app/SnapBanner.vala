/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/SnapBanner.ui")]
public class SnapBanner : Adw.Bin
{
  public static bool should_warn()
  {
    var install_env = DejaDup.InstallEnv.instance();
    return install_env.get_name() == "snap";
  }

  [GtkChild]
  unowned Adw.Banner banner;

  construct {
    banner.revealed = SnapBanner.should_warn();
  }

  [GtkCallback]
  void on_button_clicked()
  {
    var url = "https://gitlab.gnome.org/World/deja-dup/-/wikis/Snap";
    var launcher = new Gtk.UriLauncher(url);
    launcher.launch.begin(this.root as Gtk.Window, null);
  }
}
