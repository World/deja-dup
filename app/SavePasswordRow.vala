/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

// This class exists to warn the user in case we can't save passwords at all.

public class SavePasswordRow : SwitchRow
{
  construct {
    title = _("_Remember password");
    state_set.connect(on_state_set);
  }

  bool on_state_set(bool state)
  {
    if (state) {
      var window = this.root as Gtk.Window;
      if (window == null) {
        return true; // can happen if this switch wasn't finalized
      }

      DejaDup.is_secret_service_available.begin((obj, res) => {
        if (DejaDup.is_secret_service_available.end(res))
          this.state = true; // finish state set
        else {
          this.active = false; // flip switch back to unset mode
          DejaDup.run_error_dialog.begin(
            window,
            _("Cannot remember password"),
            _("Your desktop session does not support saving passwords.")
          );
        }
      });
      return true; // delay setting of state
    }

    return false;
  }
}
