/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/PassphraseDialog.ui")]
class PassphraseDialog : Adw.AlertDialog
{
  public string label {get; set;}
  public string passphrase {get; protected set;}
  public bool remember {get; protected set;}

  [GtkChild]
  unowned Adw.PasswordEntryRow entry;

  ~PassphraseDialog()
  {
    debug("Finalizing PassphraseDialog\n");
  }

  [GtkCallback]
  void entry_changed_cb()
  {
    set_response_enabled("continue", entry.text != "");
  }
}
