/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

// There are three notification IDs we send out:
// 1. "prompt": this is used to encourage using Deja Dup in first place.
//    Only sent once, if user hasn't ever opened Deja Dup.
// 2. "backup-status": this is used to update the user about an ongoing
//    backup - "it's started" or "there was a problem" or "attention needed".
//    Note that we do use this ID even for restores.
// 3. null: this is used for messages we don't intend to withdraw - only
//    currently used for "backup finished" messages, which should pile up as
//    needed and be a bit of a log for the user, if they really never clean
//    up. This also makes it harder to accidentally withdraw the final
//    "finished" message, which we have done in the past, whoops.
public class Notifications : Object
{
  public static void automatic_backup_started()
  {
    send_status(_("Starting scheduled backup"), null, NotificationPriority.LOW);
  }

  public static void automatic_backup_delayed(string reason)
  {
    send_status(_("Scheduled backup delayed"), reason);
  }

  public static void backup_finished(Gtk.Widget parent, bool success,
                                     bool cancelled, string? detail)
  {
    if (!window_is_active(parent) && success && !cancelled) {
      string title = _("Backup completed");
      var priority = NotificationPriority.LOW;

      string body = null;
      if (detail != null) {
        title = _("Backup finished");
        body = _("But not all files were successfully backed up.");
        priority = NotificationPriority.NORMAL;
      }
      else if (SnapBanner.should_warn()) {
        body = _("This snap release is deprecated and might not receive updates.");
      }

      // A "backup finished" notification shouldn't be withdrawn or replaced.
      // It is not the same as an in-progress report and we don't want to
      // withdraw it when the operation stops.
      // So first, withdraw the status one, and add a new notification with
      // a null ID so it's not withdrawable.
      withdraw_status();
      send_status(title, body, priority, null);
    }
    else if (!window_is_active(parent) && !success && !cancelled) {
      send_status(_("Backup failed"));
    }
    else {
      // We're done with this backup, no need to still talk about it
      withdraw_status();
    }
  }

  public static void restore_finished(Gtk.Widget parent, bool success,
                                      bool cancelled, string? detail)
  {
    if (!window_is_active(parent) && !success && !cancelled)
      send_status(_("Restore failed"));
  }

  public static void operation_blocked(Gtk.Widget parent, string title,
                                       string? body = null)
  {
    if (!window_is_active(parent))
      send_status(title, body);
  }

  public static void operation_unblocked()
  {
    withdraw_status();
  }

  public static void attention_needed(Gtk.Widget parent, string title,
                                      string? body = null)
  {
    if (!window_is_active(parent))
      send_status(title, body, NotificationPriority.HIGH);
  }

  public static void prompt()
  {
    DejaDup.update_prompt_time();

    var note = make_note(_("Keep your files safe by backing up regularly"),
                         _("Important documents, data, and settings can be " +
                           "protected by storing them in a backup. In the " +
                           "case of a disaster, you would be able to recover " +
                           "them from that backup."));
    note.set_default_action("app.prompt-ok");
    note.add_button(_("Don’t Show Again"), "app.prompt-cancel");
    note.add_button(_("Open Backups"), "app.prompt-ok");

    Application.get_default().send_notification("prompt", note);
  }

  public static void operation_closed()
  {
    withdraw_status();
  }

  static Notification make_note(string title, string? body)
  {
    var note = new Notification(title);
    if (body != null)
      note.set_body(body);

    // Allow overriding themed icon, because the notification daemon can't
    // always find it (e.g. snaps or local dev)
    var icon_env = Environment.get_variable("DEJA_DUP_NOTIFICATION_ICON");
    if (icon_env != null)
      note.set_icon(new FileIcon(File.new_for_path(icon_env)));
    else
      note.set_icon(new ThemedIcon(Config.ICON_NAME));

    // Set default action, otherwise the freedesktop notification backend
    // will ignore clicking on the body of a notification.
    note.set_default_action("app.show");

    return note;
  }

  static void send_status(string title, string? body = null,
    NotificationPriority priority = NotificationPriority.NORMAL,
    string? id = "backup-status")
  {
    var note = make_note(title, body);
    note.set_priority(priority);
    Application.get_default().send_notification(id, note);
  }

  static void withdraw_status()
  {
    Application.get_default().withdraw_notification("backup-status");
  }

  static bool window_is_active(Gtk.Widget parent)
  {
    var win = parent.root as Gtk.Window;
    return win != null && win.is_active && win.visible;
  }
}
