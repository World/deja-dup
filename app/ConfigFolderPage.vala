/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/ConfigFolderPage.ui")]
public class ConfigFolderPage : Adw.PreferencesPage
{
  public bool show_reset {get; set; default=false;}

  [GtkCallback]
  void on_reset_clicked()
  {
    confirm_reset.begin();
  }

  async void confirm_reset()
  {
    var dlg = new Adw.AlertDialog(
      _("Reset All Folders?"),
      _("All changes to backed up and ignored folders will be lost.")
    );
    dlg.add_response("cancel", _("_Cancel"));
    dlg.add_response("reset", _("Rese_t"));
    dlg.set_response_appearance("reset", Adw.ResponseAppearance.DESTRUCTIVE);
    dlg.default_response = "cancel";
    var choice = yield dlg.choose(this, null);
    if (choice == "reset") {
      var settings = DejaDup.get_settings();
      settings.reset(DejaDup.INCLUDE_LIST_KEY);
      settings.reset(DejaDup.EXCLUDE_LIST_KEY);
    }
  }
}
