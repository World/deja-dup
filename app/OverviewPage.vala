/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/OverviewPage.ui")]
public class OverviewPage : Gtk.Box
{
  [GtkChild]
  unowned Adw.ActionRow storage_row;
  [GtkChild]
  unowned HelpButton storage_warning_button;
  [GtkChild]
  unowned Gtk.Label storage_warning_label;

  construct {
    var watcher = DejaDup.BackendWatcher.get_instance();
    watcher.changed.connect(() => {update_storage_row.begin();});

    update_storage_row.begin();
  }

  async void update_storage_row()
  {
    var backend = DejaDup.Backend.get_default();
    storage_row.subtitle = yield backend.get_location_pretty();

    string reason;
    if (backend.is_acceptable(out reason)) {
      storage_warning_button.visible = false;
      storage_row.remove_css_class("error");
    }
    else {
      storage_warning_button.visible = true;
      storage_warning_label.label = reason;
      storage_row.add_css_class("error");
    }
  }
}
