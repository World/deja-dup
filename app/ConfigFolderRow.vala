/* -*- Mode: Vala; indent-tabs-mode: nil; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

[GtkTemplate (ui = "/org/gnome/DejaDup/ConfigFolderRow.ui")]
public class ConfigFolderRow : Adw.ActionRow
{
  public File file {get; set;}
  public bool check_access {get; set;}

  public signal void remove_clicked();

  [GtkChild]
  unowned HelpButton warning_button;
  [GtkChild]
  unowned Gtk.Label warning_label;

  construct {
    notify["file"].connect(() => {update_row.begin();});
    notify["check-access"].connect(() => {update_row.begin();});
    update_row.begin();
  }

  async void update_row()
  {
    title = file == null ? "" : yield DejaDup.get_nickname(file);

    var has_warning = false;
    if (check_access && file != null) {
      var install_env = DejaDup.InstallEnv.instance();

      // Keep this list of rules in sync with OperationBackup's
      // filter on include/excludes.
      if (!file.is_native()) {
        warning_label.label = _("This folder cannot be backed up because it is a network folder.");
        has_warning = true;
      } else if (!install_env.is_file_available(file)) {
        warning_label.label = _("This folder cannot be backed up because Backups does not have access to it.");
        has_warning = true;
      }
    }

    if (has_warning) {
      warning_button.visible = true;
      add_css_class("error");
    } else {
      warning_button.visible = false;
      remove_css_class("error");
    }
  }

  [GtkCallback]
  void on_remove_clicked()
  {
    remove_clicked();
  }
}
