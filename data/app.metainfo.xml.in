<?xml version="1.0" encoding="UTF-8"?>
<!-- -*- Mode: XML; indent-tabs-mode: nil; tab-width: 2 -*- -->
<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

<component type="desktop-application">
  <id>@appid@</id>

  <name>Déjà Dup Backups</name>
  <summary>Protect yourself from data loss</summary>
  <description>
    <p>Déjà Dup is a simple backup tool. It hides the complexity of backing up the Right Way (encrypted, off-site, and regular) and uses Restic &amp; Duplicity behind the scenes.</p>
    <ul>
      <li>Support for local, remote, or cloud backup locations such as Google Drive</li>
      <li>Securely encrypts and compresses your data</li>
      <li>Incrementally backs up, letting you restore from any particular backup</li>
      <li>Schedules regular backups</li>
      <li>Integrates well into your GNOME desktop</li>
    </ul>
    <p>Déjà Dup focuses on ease of use and recovering from personal, accidental data loss. If you need a full system backup or an archival program, you may prefer other backup apps.</p>
  </description>

  <recommends>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </recommends>

  <requires>
    <display_length compare="ge">360</display_length>
  </requires>

  <provides>
    <binary>deja-dup</binary>
    <id>deja-dup.desktop</id>
    <id>deja-dup-preferences.desktop</id>
    <id>io.snapcraft.deja-dup-FWBFbkKw7QOrYcJDRBwoDT2JNsavuet8</id>
    <id>org.gnome.DejaDup.desktop</id>
  </provides>

  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>CC-BY-SA-4.0</metadata_license>
  <launchable type="desktop-id">@appid@.desktop</launchable>
  <translation type="gettext">@gettext@</translation>
  <content_rating type="oars-1.1" />

  <url type="homepage">https://apps.gnome.org/DejaDup/</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/deja-dup/-/issues</url>
  <url type="faq">https://gitlab.gnome.org/World/deja-dup/-/wikis/FAQ</url>
  <url type="help">https://gitlab.gnome.org/World/deja-dup/-/wikis/home</url>
  <url type="donation">https://liberapay.com/DejaDup</url>
  <url type="translate">https://l10n.gnome.org/module/deja-dup/</url>
  <url type="contact">https://matrix.to/#/#deja-dup:gnome.org</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/deja-dup</url>
  <url type="contribute">https://welcome.gnome.org/app/DejaDup/</url>

  <developer id="name.mterry">
    <name translate="no">Michael Terry</name>
  </developer>

  <branding>
    <!--
     See https://gnome.pages.gitlab.gnome.org/gnome-software/help/C/software-metadata.html
     for tips on defining these and ensuring good contrast.

     Ideal goals:
     - Dark theme color should have:
       - 7:1 contrast with #ffffff (Adw window_fg_color)
       - 3:1 contrast with #9a9996 (icon inner gray)
     - Light theme color should have:
       - 7:1 contrast with #2e2e2e (Adw window_fg_color)
       - 3:1 contrast with #5e5c64ff (icon outer gray)

     We're currently using some calm blue colors, which seem like
     pleasant backgrounds against our gray icon.
    -->
    <color type="primary" scheme_preference="dark">#304d64</color>
    <color type="primary" scheme_preference="light">#7bc9ff</color>
  </branding>

  <custom>
    <!-- copies of above <branding> values; remove after support is widespread -->
    <value key="GnomeSoftware::key-colors">[(48, 77, 100), (123, 201, 255)]</value>
  </custom>

  <!-- Run the org.gnome.DejaDupScreenshot flatpak to recreate these screenshots -->
  <!-- I use commit hashes instead of tags here because with tags,
       I found that testing a flathub build before creating the tag would fail as a result,
       and that test is a useful pre-release check. So just make sure that you update the
       screenshots first, then edit the link in a second commit once you know the hash. -->
  <screenshots>
    <screenshot type="default" environment="gnome">
      <image type="source" width="772" height="582">https://gitlab.gnome.org/World/deja-dup/raw/efd499b6e5dc7460500cc8eccf1fc77c93d92895/data/screenshots/1-main.png</image>
      <caption>Back up your files automatically — set it and forget it</caption>
    </screenshot>
    <screenshot environment="gnome">
      <image type="source" width="772" height="582">https://gitlab.gnome.org/World/deja-dup/raw/efd499b6e5dc7460500cc8eccf1fc77c93d92895/data/screenshots/2-restore.png</image>
      <caption>Restoring is a breeze — browse your files and select which to restore</caption>
    </screenshot>
    <screenshot environment="gnome">
      <image type="source" width="772" height="582">https://gitlab.gnome.org/World/deja-dup/raw/efd499b6e5dc7460500cc8eccf1fc77c93d92895/data/screenshots/3-preferences.png</image>
      <caption>Back up to the cloud, a network server, or a local drive</caption>
    </screenshot>
    <screenshot environment="gnome">
      <image type="source" width="772" height="582">https://gitlab.gnome.org/World/deja-dup/raw/efd499b6e5dc7460500cc8eccf1fc77c93d92895/data/screenshots/4-folders.png</image>
      <caption>Take control of your backup by choosing exactly which folders to include</caption>
    </screenshot>
  </screenshots>

  <releases>
    <release version="47.0" date="2024-09-29">
      <description>
        <p>UI and Restic support improvements</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/47.0</url>
    </release>
    <release version="46.1" date="2024-06-23">
      <description>
        <p>Fix connecting to WebDAV servers</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/46.1</url>
    </release>
    <release version="46.0" date="2024-06-18">
      <description>
        <p>Use modern Adwaita dialogs and fix lots of bugs</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/46.0</url>
    </release>
    <release version="46~beta" date="2024-03-24" type="development">
      <description>
        <p>Use modern Adwaita dialogs and various small UI improvements</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/46.beta</url>
    </release>
    <release version="45.2" date="2023-12-22">
      <description>
        <p>Fixed not being prompted for packagekit installs during a restore</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/45.2</url>
    </release>
    <release version="45.1" date="2023-09-22">
      <description>
        <p>Update UI to use the new flat header style (re-released with a build fix)</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/45.1</url>
    </release>
    <release version="45.0" date="2023-09-21">
      <description>
        <p>Update UI to use the new flat header style</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/45.0</url>
    </release>
    <release version="44.2" date="2023-05-21">
      <description>
        <p>Add support for newer Duplicity versions and some minor UI tweaks</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/44.2</url>
    </release>
    <release version="44.1" date="2023-03-24">
      <description>
        <p>Stop a backup up front if the storage location does not have enough space for a new backup.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/44.1</url>
    </release>
    <release version="44.0" date="2022-11-25">
      <description>
        <p>Modernizes the UI in a few places and enables compression for new Restic backups.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/44.0</url>
    </release>
    <release version="43.4" date="2022-06-21">
      <description>
        <p>Fixes some quality of life issues, like notifying about power saver mode delaying your backup and fitting on small screens better.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.4</url>
    </release>
    <release version="43.3" date="2022-05-17">
      <description>
        <p>Update the Google/Microsoft authentication flow to a more secure version.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.3</url>
    </release>
    <release version="43.2" date="2022-02-05">
      <description>
        <p>Fix a crash when picking a mount as a Local Folder destination.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.2</url>
    </release>
    <release version="43.1" date="2022-01-05">
      <description>
        <p>Increases default window size, to avoid being too small.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.1</url>
    </release>
    <release version="43.0" date="2022-01-01">
      <description>
        <p>Adds support for Microsoft OneDrive, experimental opt-in support for Restic, and a visual refresh.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.0</url>
    </release>
    <release version="43~beta" date="2021-11-26" type="development">
      <description>
        <p>Adds a couple extra automatic schedule options and some bug fixes.</p>
      </description>
      <url>https://gitlab.gnome.org/World/deja-dup/-/releases/43.beta</url>
    </release>
    <release version="43~alpha" date="2021-09-22" type="development">
      <description>
        <p>Adds support for Microsoft OneDrive, experimental opt-in support for Restic, and some UI cleanup.</p>
      </description>
    </release>
    <release version="42.8" date="2021-08-11">
      <description>
        <p>Fix scheduled backups being skipped if a previous password prompt was ignored.</p>
      </description>
    </release>
    <release version="42.7" date="2021-01-13">
      <description>
        <p>Fix descending into a folder while searching in the browse and restore view.</p>
      </description>
    </release>
    <release version="42.6" date="2020-11-21">
      <description>
        <p>Fix a possible crash when searching over unicode filenames.</p>
      </description>
    </release>
    <release version="42.5" date="2020-10-21">
      <description>
        <p>Fix a few issues with restoring files that have unusual characters in their filenames, plus some other small UI improvements and bug fixes.</p>
      </description>
    </release>
    <release version="42.4" date="2020-09-11">
      <description>
        <p>Hotfix for a bug that prevented restoring from removable drives.</p>
      </description>
    </release>
    <release version="42.3" date="2020-09-09">
      <description>
        <p>Improves detection of encrypted drives, shows the browse and restore interface during fresh-install restores, and uses desktop notifications when user attention is needed.</p>
      </description>
    </release>
    <release version="42.2" date="2020-08-10">
      <description>
        <p>Fixes a few edge cases of not mounting the storage location drive before backing up.</p>
      </description>
    </release>
    <release version="42.1" date="2020-07-15">
      <description>
        <p>Fixes automatic backups for removable drives not firing off like they should, plus a few UI tweaks.</p>
      </description>
    </release>
    <release version="42.0" date="2020-06-24">
      <description>
        <p>A whole new backup browse and restore interface has been added. Preferences got a glow-up too.</p>
      </description>
    </release>
    <release version="41.3" date="2020-06-22" type="development">
      <description>
        <p>Warns you if you're trying to restore files that you can't write to.</p>
      </description>
    </release>
    <release version="41.2" date="2020-06-08" type="development">
      <description>
        <p>Fixes a bug preventing some restores from new browser.</p>
      </description>
    </release>
    <release version="41.1" date="2020-05-27" type="development">
      <description>
        <p>Adds an in-app restore browser.</p>
      </description>
    </release>
    <release version="41.0" date="2020-05-19" type="development">
      <description>
        <p>Redesigned UI and dropped support for deprecated cloud storage locations.</p>
      </description>
    </release>
    <release version="40.7" date="2020-06-14">
      <description>
        <p>Fixes a bug that prevented restoring from Google Drive accounts if you haven't backed up yet.</p>
      </description>
    </release>
    <release version="40.6" date="2019-12-08">
      <description>
        <p>Fixes a bug that prevented backing up to Google Drive accounts with unlimited quotas.</p>
      </description>
    </release>
    <release version="40.5" date="2019-11-23">
      <description>
        <p>Fixes a bug that prevented backing up to Google Drive in some rare situations.</p>
      </description>
    </release>
    <release version="40.4" date="2019-11-14">
      <description>
        <p>Fixes a bug that prevented the first login to Google Drive.</p>
      </description>
    </release>
    <release version="40.3" date="2019-11-13">
      <description>
        <p>Fixes a bug that prevented resuming a full backup.</p>
      </description>
    </release>
    <release version="40.2" date="2019-10-23">
      <description>
        <p>Fixes 2038 date problems by using 64-bit dates internally.</p>
      </description>
    </release>
    <release version="40.1" date="2019-04-15"/>
    <release version="40.0" date="2019-04-15"/>
    <release version="39.1" date="2019-04-05" type="development"/>
    <release version="39.0" date="2019-03-28" type="development"/>
    <release version="38.4" date="2019-02-22"/>
    <release version="38.3" date="2019-01-16"/>
    <release version="38.2" date="2019-01-05"/>
    <release version="38.1" date="2018-12-07"/>
    <release version="38.0" date="2018-04-09"/>
    <release version="37.1" date="2018-01-02" type="development"/>
    <release version="37.0" date="2017-11-18" type="development"/>
    <release version="36.3" date="2017-10-20"/>
    <release version="36.2" date="2017-10-02"/>
    <release version="36.1" date="2017-09-14"/>
    <release version="36.0" date="2017-09-11"/>
    <release version="35.6" date="2017-08-31" type="development"/>
    <release version="35.5" date="2017-08-22" type="development"/>
    <release version="35.4" date="2017-08-17" type="development"/>
    <release version="35.3" date="2017-08-04" type="development"/>
    <release version="35.2" date="2017-07-30" type="development"/>
    <release version="35.1" date="2017-07-28" type="development"/>
    <release version="35.0" date="2017-07-28" type="development"/>
    <release version="34.3" date="2016-11-27"/>
    <release version="34.2" date="2016-04-07"/>
    <release version="34.1" date="2015-11-29"/>
    <release version="34.0" date="2015-04-10"/>
    <release version="32.0" date="2014-09-20"/>
    <release version="30.0" date="2014-03-28"/>
    <release version="29.5" date="2014-01-30" type="development"/>
    <release version="29.4" date="2014-01-10" type="development"/>
    <release version="29.1" date="2013-11-07" type="development"/>
    <release version="28.0" date="2013-09-23"/>
    <release version="27.3.1" date="2013-06-17" type="development"/>
    <release version="27.3" date="2013-06-17" type="development"/>
    <release version="26.0" date="2013-06-12"/>
    <release version="25.5" date="2013-02-04" type="development"/>
    <release version="25.3" date="2012-12-18" type="development"/>
    <release version="25.1.1" date="2012-11-05" type="development"/>
    <release version="25.1" date="2012-10-25" type="development"/>
    <release version="24.0" date="2012-09-24"/>
    <release version="23.90" date="2012-08-21" type="development"/>
    <release version="23.4" date="2012-07-27" type="development"/>
    <release version="23.2" date="2012-06-05" type="development"/>
    <release version="22.1" date="2012-04-16"/>
    <release version="22.0" date="2012-03-22"/>
    <release version="21.90" date="2012-02-23" type="development"/>
    <release version="21.4" date="2012-01-15" type="development"/>
    <release version="21.2" date="2011-11-21" type="development"/>
    <release version="21.1" date="2011-10-23" type="development"/>
    <release version="20.2" date="2011-11-16"/>
    <release version="20.1" date="2011-10-17"/>
    <release version="20.0" date="2011-09-26"/>
    <release version="19.92" date="2011-09-15" type="development"/>
    <release version="19.91" date="2011-08-31" type="development"/>
    <release version="19.90" date="2011-08-22" type="development"/>
    <release version="19.5" date="2011-08-15" type="development"/>
    <release version="19.4" date="2011-07-26" type="development"/>
    <release version="19.3" date="2011-06-24" type="development"/>
    <release version="19.2.2" date="2011-06-14" type="development"/>
    <release version="19.2.1" date="2011-06-13" type="development"/>
    <release version="19.2" date="2011-06-13" type="development"/>
    <release version="19.1" date="2011-05-07" type="development"/>
    <release version="18.2.1" date="2011-06-10"/>
    <release version="18.2" date="2011-06-09"/>
    <release version="18.1.1" date="2011-04-15"/>
    <release version="18.1" date="2011-04-12"/>
    <release version="18.0" date="2011-04-05"/>
    <release version="17.92" date="2011-03-24" type="development"/>
    <release version="17.91" date="2011-03-15" type="development"/>
    <release version="17.90" date="2011-02-22" type="development"/>
    <release version="17.6" date="2011-02-04" type="development"/>
    <release version="17.5" date="2011-01-11" type="development"/>
    <release version="17.4" date="2010-12-23" type="development"/>
    <release version="17.3" date="2010-12-04" type="development"/>
    <release version="17.2" date="2010-11-17" type="development"/>
    <release version="17.1" date="2010-10-20" type="development"/>
    <release version="17.0" date="2010-10-03" type="development"/>
    <release version="16.1.1" date="2010-11-17"/>
    <release version="16.1" date="2010-11-17"/>
    <release version="16.0" date="2010-09-27"/>
    <release version="15.92" date="2010-09-11" type="development"/>
    <release version="15.5" date="2010-07-12" type="development"/>
    <release version="15.3" date="2010-06-05" type="development"/>
    <release version="15.1" date="2010-05-03" type="development"/>
    <release version="14.2" date="2010-06-05"/>
    <release version="14.1" date="2010-05-01"/>
    <release version="14.0.3" date="2010-04-18"/>
    <release version="14.0.2" date="2010-04-06"/>
    <release version="14.0.1" date="2010-04-03"/>
    <release version="14.0" date="2010-03-28"/>
    <release version="13.92" date="2010-03-08" type="development"/>
    <release version="13.91" date="2010-02-22" type="development"/>
    <release version="13.6" date="2010-01-24" type="development"/>
    <release version="13.5" date="2010-01-09" type="development"/>
    <release version="13.4" date="2009-12-19" type="development"/>
    <release version="13.3" date="2009-11-29" type="development"/>
    <release version="11.1" date="2009-11-16"/>
    <release version="11.0" date="2009-10-29"/>
    <release version="10.3" date="2009-11-16"/>
    <release version="10.2" date="2009-10-09"/>
    <release version="10.1" date="2009-06-13"/>
    <release version="10.0" date="2009-06-05"/>
    <release version="9.3" date="2009-05-30"/>
    <release version="9.2" date="2009-05-10"/>
    <release version="9.1" date="2009-04-27"/>
    <release version="9.0" date="2009-04-26"/>
    <release version="8.1" date="2009-04-03"/>
    <release version="8.0" date="2009-03-29"/>
    <release version="7.4" date="2009-02-11"/>
    <release version="7.3" date="2009-02-03"/>
    <release version="7.2" date="2009-02-02"/>
    <release version="7.1" date="2009-01-29"/>
    <release version="7.0" date="2009-01-29"/>
    <release version="6.0" date="2009-01-19"/>
    <release version="5.2" date="2009-01-07"/>
    <release version="5.1" date="2009-01-07"/>
    <release version="5.0" date="2009-01-06"/>
    <release version="4.0" date="2008-12-30"/>
    <release version="3.0" date="2008-12-04"/>
    <release version="2.0" date="2008-11-22"/>
    <release version="1.0" date="2008-10-16"/>
  </releases>

</component>
