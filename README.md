<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# Déjà Dup Backups

Déjà Dup is a simple backup tool. It hides the complexity of backing up the
Right Way (encrypted, off-site, and regular) and uses
[Restic](https://restic.net/) & [Duplicity](https://duplicity.gitlab.io/)
behind the scenes.

 * Support for local, remote, or cloud backup locations such as Google Drive
 * Securely encrypts and compresses your data
 * Incrementally backs up, letting you restore from any particular backup
 * Schedules regular backups
 * Integrates well into your GNOME desktop

Déjà Dup focuses on ease of use and personal, accidental data loss.
If you need a full system backup or an archival program, you may prefer other
backup apps.

[![Get it on Flathub](https://flathub.org/api/badge?locale=en)](https://flathub.org/apps/org.gnome.DejaDup)

## Building

If you are hacking on Déjà Dup, see [CONTRIBUTING.md](CONTRIBUTING.md).

Or if you are packaging Déjà Dup for a distribution, see
[PACKAGING.md](PACKAGING.md) for extra tips.

## Links

 * [Homepage](https://apps.gnome.org/DejaDup/)
 * [Get involved](https://welcome.gnome.org/app/DejaDup/)
 * [Chat room](https://matrix.to/#/#deja-dup:gnome.org)
 * [Forums](https://discourse.gnome.org/tags/c/applications/7/deja-dup)

