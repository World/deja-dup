<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# Welcome

This guide is aimed at developers looking to build Déjà Dup locally and maybe contribute back a patch.

Thank you so much for helping out! That's so nice.

If you encounter a problems or just want to run an idea by us first before spending much time on it, please get in touch in our [chat room][chat].

The Déjà Dup project follows the [GNOME Code of Conduct](https://conduct.gnome.org/).
Please be respectful and considerate.

[chat]: https://matrix.to/#/#deja-dup:gnome.org

# Building from a Source Release

This is recommended if you are a downstream packager of stable releases.
Though if you *are* a downstream packager, please read
[PACKAGING.md](PACKAGING.md) instead, as it has more relevant tips.

If you have downloaded this source from a tarball release (that is, a file like `.tar.bz2` or `.zip`),
you can use standard meson commands like:
 * `meson --buildtype=release my-build-directory`
 * `meson compile -C my-build-directory`

See the [meson documentation](https://mesonbuild.com/) for more guidance.
And look at [meson_options.txt](meson_options.txt) for all the extra build options you can set.

# Building from a Git Clone

This is recommended if you intend to contribute back a patch.

## Using GNOME Builder

This is the easiest option.
Read the [online instructions](https://welcome.gnome.org/app/DejaDup/#working-on-the-code)
for setting up Builder.

Builder will install dependencies, build, and run Déjà Dup for you.

Note: you should probably also separately
[install the nightly flatpak build for Déjà Dup](https://welcome.gnome.org/app/DejaDup/#installing-a-nightly-build),
since that will provide some better system integration for when
you run the app inside Builder.

## Using Meson

Déjà Dup is also a standard Meson project, so you can use all the usual commands.

You'll need to install a bunch of dependencies (and might need very new versions).
Running in a [toolbox](https://containertoolbx.org/) or similar container might help.

Meson setup will complain about each missing dependency,
and you can install them from your distro as you go.

[PACKAGING.md](PACKAGING.md) will also give some pointers at required versions.

- `meson setup _build`
- `meson compile -C _build`
- `meson devenv -C _build deja-dup`
- `meson test -C _build`

# Folder Layout
 * libdeja: non-GUI library that wraps policy and hides complexity of duplicity
 * app: GNOME UI for libdeja
 * monitor: the deja-dup-monitor user daemon
 * data: shared schemas, icons, etc

# Copyright

If you are making a [substantial patch](https://www.gnu.org/prep/maintain/html_node/Legally-Significant.html) (adding ~15 lines or more), add yourself to the top of the changed file in a new copyright line.

# Project Assets

If the maintainers get hit by a bus, these are the various pieces of the administrative puzzle:

* [Main project page](https://gitlab.gnome.org/World/deja-dup)
* [Wiki](https://gitlab.gnome.org/World/deja-dup/-/wikis)
* [Discourse tag](https://discourse.gnome.org/tag/deja-dup)
* [Snap packaging](https://github.com/deja-dup/snap) (plus the store account)
* [Flathub packaging](https://github.com/flathub/org.gnome.DejaDup)
* Google Drive API account (client ID is in `meson_options.txt`)
* Microsoft OneDrive API account (client ID is in `meson_options.txt`)
* dejadup.org domain (redirects to wiki above, only currently used for our Google API account, which requires a domain)
* [Liberapay team](https://liberapay.com/DejaDup)
* [Old project page](https://launchpad.net/deja-dup)
* [Old GNOME wiki](https://wiki.gnome.org/Apps/DejaDup?action=LikePages)
