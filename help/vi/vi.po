# Vietnamese translation for deja-dup
# Copyright (c) 2017 Rosetta Contributors and Canonical Ltd 2017
# SPDX-License-Identifier: CC-BY-SA-4.0
#
msgid ""
msgstr ""
"Project-Id-Version: deja-dup\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-07-05 21:49-0400\n"
"PO-Revision-Date: 2019-02-23 09:39+0000\n"
"Last-Translator: Michael Terry <mike@mterry.name>\n"
"Language-Team: Vietnamese <vi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-07-07 06:23+0000\n"
"X-Generator: Launchpad (build ffd32ad7291fe66b5578d7c1407aaae58d1e0170)\n"
"Language: vi\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Lê Trường An <https://launchpad.net/~truongan>, "

#. (itstool) path: info/desc
#: C/contribute.page:13
msgid "Help make <app>Déjà Dup</app> better"
msgstr "Giúp cải thiện <app>Déjà Dup</app> tốt hơn nữa"

#. (itstool) path: page/title
#: C/contribute.page:16
msgid "Getting Involved"
msgstr "Tham gia"

#. (itstool) path: page/p
#: C/contribute.page:18
msgid ""
"So you want to help make <app>Déjà Dup</app> even better? Excellent! Here "
"are some suggestions."
msgstr ""
"Bạn muốn giúp cải thiện <app>Déjà Dup</app> tốt hơn nữa? Thật tuyệt! Đây là "
"một số đề nghị."

#. (itstool) path: section/title
#: C/contribute.page:21
msgid "Report Issues"
msgstr "Báo cáo vấn đề"

#. (itstool) path: section/p
#: C/contribute.page:23
msgid ""
"Did you see a flaw? Do you have an idea for a new feature? Simply <link "
"href=\"https://bugs.launchpad.net/deja-dup/+filebug\">report it as a "
"bug</link>. Be patient for a reply."
msgstr ""
"Bạn phát hiện một thiếu sót? Bạn có ý tưởng cho một tính năng mới? Hãy <link "
"href=\"https://bugs.launchpad.net/deja-dup/+filebug\">lập một báo cáo "
"lỗi</link>. Và kiên nhẫn chờ phản hồi."

#. (itstool) path: section/p
#: C/contribute.page:25
msgid "Note that bug reports should be in English only."
msgstr "Lưu ý rằng báo cáo lỗi chỉ được viết bằng Tiếng Anh."

#. (itstool) path: section/title
#: C/contribute.page:29
msgid "Talk to Us"
msgstr "Trò chuyện với chúng tôi"

#. (itstool) path: section/p
#: C/contribute.page:31
msgid ""
"We don’t bite. Send an email to the <link href=\"mailto:deja-dup-"
"list@gnome.org\">mailing list</link>."
msgstr ""
"Chúng tôi rất thân thiện. Hãy gửi thư đến <link href=\"mailto:deja-dup-"
"list@gnome.org\">hộp thư chung</link>."

#. (itstool) path: section/p
#: C/contribute.page:33
msgid ""
"Or <link href=\"https://mail.gnome.org/mailman/listinfo/deja-dup-"
"list\">subscribe to the list</link>. The list is very low traffic."
msgstr ""
"Hoặc <link href=\"https://mail.gnome.org/mailman/listinfo/deja-dup-"
"list\">đăng kí với hộp thư chung</link>. Hộp thư có lượng truy cập rất thấp."

#. (itstool) path: section/p
#: C/contribute.page:35
msgid "Note that the list is English only."
msgstr "Lưu ý là hộp thư chỉ dùng Tiếng Anh."

#. (itstool) path: section/title
#: C/contribute.page:39
msgid "Translating"
msgstr "Dịch"

#. (itstool) path: section/p
#: C/contribute.page:41
msgid ""
"Do you speak both English and another language? Good! You can help translate "
"<app>Déjà Dup</app> through <link "
"href=\"https://translations.launchpad.net/deja-dup\">a web interface</link>. "
"It’s really quite easy and is very appreciated."
msgstr ""
"Bạn biết cả Tiếng Anh và ngôn ngữ khác? Thật tốt! Bạn có thể giúp dịch "
"<app>Déjà Dup</app> thông qua <link "
"href=\"https://translations.launchpad.net/deja-dup\">giao diện web</link>. "
"Thật sự dễ dàng và được đánh giá cao."

#. (itstool) path: section/title
#. (itstool) path: page/title
#: C/contribute.page:45
#: C/support.page:16
msgid "Support"
msgstr "Hỗ trợ"

#. (itstool) path: section/p
#: C/contribute.page:47
msgid ""
"Do you like helping people? <link href=\"https://answers.launchpad.net/deja-"
"dup\">Answer user questions</link> and support requests."
msgstr ""
"Bạn muốn giúp đỡ người khác? <link href=\"https://answers.launchpad.net/deja-"
"dup\">Trả lời những câu hỏi của người dùng</link> và yêu cầu hỗ trợ."

#. (itstool) path: section/p
#: C/contribute.page:49
msgid ""
"This is another great way for non-English speakers to get involved, as you "
"can answer questions posted in your language."
msgstr ""
"Đây là một cách hay cho những ai không nói Tiếng Anh muốn tham gia, bạn có "
"thể đặt câu hỏi bằng ngôn ngữ của bạn."

#. (itstool) path: section/title
#: C/contribute.page:53
msgid "Coding"
msgstr "Lập trình"

#. (itstool) path: section/p
#: C/contribute.page:55
msgid ""
"There are always more bugs to fix. If you want to find out what needs doing, "
"talk to us on the mailing list as described above."
msgstr ""
"Luôn có lỗi để sửa. Nếu bạn muốn biết phải làm những gì, hãy nói cho chúng "
"tôi biết trên hộp thư chung như đã mô tả ở trên."

#. (itstool) path: info/desc
#: C/credits.page:13
msgid "Meet the team"
msgstr ""

#. (itstool) path: page/title
#: C/credits.page:16
msgid "Credits"
msgstr "Đóng góp"

#. (itstool) path: section/title
#: C/credits.page:19
msgid "Development"
msgstr "Phát triển"

#. (itstool) path: section/title
#: C/credits.page:27
msgid "Art"
msgstr "Mỹ thuật"

#. (itstool) path: section/title
#: C/credits.page:36
msgid "Documentation"
msgstr "Tài liệu"

#. (itstool) path: section/title
#: C/credits.page:43
msgid "Translation"
msgstr "Bản dịch"

#. (itstool) path: info/title
#: C/index.page:14
msgctxt "link"
msgid "Backups Help"
msgstr ""

#. (itstool) path: info/title
#: C/index.page:15
msgctxt "text"
msgid "Backups Help"
msgstr ""

#. (itstool) path: license/p
#: C/index.page:21
msgid "Creative Commons Attribution-Share Alike 4.0 Unported License"
msgstr ""

#. (itstool) path: page/title
#: C/index.page:24
msgid "Backups Help"
msgstr ""

#. (itstool) path: section/title
#: C/index.page:27
msgid "Backing Up"
msgstr "Sao lưu"

#. (itstool) path: section/title
#: C/index.page:31
msgid "Restoring Files"
msgstr "Phục hồi tập tin"

#. (itstool) path: section/title
#: C/index.page:35
msgid "About <app>Backups</app>"
msgstr ""

#. (itstool) path: info/desc
#: C/license.page:11
msgid "Know your rights"
msgstr "Quyền lợi của bạn"

#. (itstool) path: page/title
#: C/license.page:14
msgid "License"
msgstr "Giấy phép"

#. (itstool) path: page/p
#: C/license.page:15
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 3 of the License, or (at your option) "
"any later version."
msgstr ""
"Chương trình này là phần mềm tự do; bạn có thể phát hành lại nó và/hoặc sửa "
"đổi nó với điều kiện của Giấy Phép Công Cộng GNU được xuất bản bởi Tổ Chức "
"Phần Mềm Tự Do; hoặc phiên bản 3 của Giấy Phép này, hoặc (tùy chọn) bất kỳ "
"phiên bản mới hơn."

#. (itstool) path: page/p
#: C/license.page:16
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for "
"more details."
msgstr ""

#. (itstool) path: page/p
#: C/license.page:17
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program. If not, see <link "
"href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</link>."
msgstr ""
"Bạn phải nhận được một bản sao của Giấy phép Công cộng chung GNU kèm theo "
"chương trình này. Nếu không, xem <link "
"href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</link>."

#. (itstool) path: info/desc
#: C/prefs.page:13
msgid "Adjust your backup preferences"
msgstr ""

#. (itstool) path: page/title
#: C/prefs.page:16
msgid "Preferences"
msgstr ""

#. (itstool) path: page/p
#: C/prefs.page:18
msgid ""
"To review your backup preferences, press the menu button in the top-right "
"corner of the window and select <guiseq><gui "
"style=\"menuitem\">Preferences</gui></guiseq>."
msgstr ""

#. (itstool) path: item/title
#: C/prefs.page:22
msgid "<gui>Storage location</gui>"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:23
msgid ""
"The storage location is where <app>Déjà Dup</app> will put its copies of "
"your files so that they can be restored later."
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:24
msgid ""
"Note that backups can be quite large, so make sure you have enough free disk "
"space."
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:25
msgid ""
"If you'd like to use an external drive as a storage location, plug it in and "
"it will show up in the list."
msgstr ""

#. (itstool) path: note/p
#: C/prefs.page:26
msgid ""
"While you can choose a local folder for your backups, this is not "
"recommended. If your hardware fails, you will lose both your original data "
"and your backups all at once."
msgstr ""

#. (itstool) path: item/title
#: C/prefs.page:29
msgid "<gui>Keep Backups</gui>"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:30
msgid ""
"Choose the minimum amount of time to keep backup files. If you find that the "
"backup files are taking up too much space, you may want to decrease this "
"duration. Due to implementation details, files may be kept a bit longer than "
"the chosen time. But no files will be deleted early."
msgstr ""
"Chọn khoảng thời gian ngắn nhất để lưu giữ các bản sao lưu. Nếu bạn thấy các "
"bản sao lưu chiếm dung lượng quá lớn, bạn có thể giảm thời gian xuống. Thực "
"tế thì các tập tin có thể được lưu lâu hơn đã định một chút. Nhưng sẽ không "
"có tập tin nào bị xóa sớm hơn."

#. (itstool) path: note/p
#: C/prefs.page:31
msgid ""
"Backups are kept forever by default, but still may be deleted earlier if the "
"storage location begins to run out of space."
msgstr ""

#. (itstool) path: item/title
#: C/prefs.page:34
msgid "<gui>Back Up Automatically</gui>"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:35
msgid ""
"Turn this option on to have <app>Déjà Dup</app> automatically back up for "
"you. This is recommended so that you don’t forget to do it yourself. Backups "
"are more useful the more recent they are, so it is important to back up "
"regularly."
msgstr ""

#. (itstool) path: item/title
#: C/prefs.page:38
msgid "<gui>Folders to Back Up</gui>"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:39
msgid ""
"Choose a list of folders to save in your backup. Press the <gui "
"style=\"button\">Add</gui> or <gui style=\"button\">Remove</gui> buttons to "
"modify the list. If you are only interested in backing up your own data, the "
"default of <gui>Home</gui> is sufficient."
msgstr ""

#. (itstool) path: item/title
#: C/prefs.page:42
msgid "<gui>Folders to Ignore</gui>"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:43
msgid ""
"Choose a list of folders to not save in your backup. Press the <gui "
"style=\"button\">Add</gui> or <gui style=\"button\">Remove</gui> buttons to "
"modify the list."
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:44
msgid ""
"Some of your data may be large and not very important to you. In that case, "
"you can save yourself some time and space by not backing them up."
msgstr ""
"Vài thứ trong số dữ liệu của bạn không quan trọng nhưng lại có dung lượng "
"lớn. Trong trường hợp đó, bạn có thể tự tay lưu chúng mà không sao lưu."

#. (itstool) path: note/p
#: C/prefs.page:46
msgid "Some locations are always ignored by default:"
msgstr ""

#. (itstool) path: p/span
#: C/prefs.page:55
msgid "(which by default also ignores <_:file-1/>)"
msgstr ""

#. (itstool) path: item/p
#: C/prefs.page:64
msgid "Any folder with a <_:file-1/> or <_:file-2/> file in it."
msgstr ""

#. (itstool) path: info/desc
#: C/restore-full.page:11
msgid "Recover from total system failure"
msgstr "Phục hồi từ hệ thống bị hỏng nặng"

#. (itstool) path: page/title
#: C/restore-full.page:14
msgid "Full System Restore"
msgstr "Phục hồi hệ thống hoàn toàn"

#. (itstool) path: section/title
#: C/restore-full.page:16
msgid "Setup"
msgstr "Cài đặt"

#. (itstool) path: item/p
#: C/restore-full.page:18
msgid ""
"Get your system to a fresh state. For example, a fresh install or a new "
"account on an old computer. Don’t worry about getting comfortable and "
"customizing settings yet. Wait until after you restore your previous "
"settings to change anything, as any customizations you make now may get "
"overridden once you restore."
msgstr ""

#. (itstool) path: item/p
#: C/restore-full.page:19
msgid ""
"Next, you need to install <app>Déjà Dup</app>. Your distribution vendor may "
"package it for you. Otherwise, install it from flathub or the snap store."
msgstr ""

#. (itstool) path: section/title
#: C/restore-full.page:23
msgid "Restore"
msgstr "Phục hồi"

#. (itstool) path: item/p
#: C/restore-full.page:25
msgid "Open <gui>Backups</gui>."
msgstr ""

#. (itstool) path: item/p
#: C/restore-full.page:26
msgid ""
"Click on the <gui style=\"button\">Restore From a Previous Backup</gui> "
"button on the <gui>Overview</gui> page."
msgstr ""

#. (itstool) path: item/p
#: C/restore-full.page:27
msgid ""
"A <gui>Restore</gui> dialog will appear asking where your backup files are "
"stored (your <gui>Backup location</gui>)."
msgstr ""
"Hộp thoại <gui>Phục hồi</gui> sẽ xuất hiện để hỏi bạn về <gui>Vị trí lưu bản "
"sao lưu</gui>."

#. (itstool) path: item/p
#: C/restore-full.page:28
msgid ""
"Click <gui style=\"button\">Forward</gui> and wait for <app>Déjà Dup</app> "
"to scan the backup location."
msgstr ""
"Nhấn vào nút <gui style=\"button\">Tiếp theo</gui> và đợi <app>Déjà "
"Dup</app> quét vị trí lưu bản sao lưu."

#. (itstool) path: item/p
#: C/restore-full.page:29
msgid ""
"Choose the date you want to restore from. Usually you can just leave this "
"alone, as the default is the most recent backup."
msgstr ""
"Chọn ngày mà bạn muốn phục hồi. Thường thì bạn không cần bận tâm, ngày sao "
"lưu gần nhất sẽ được chọn."

#. (itstool) path: item/p
#: C/restore-full.page:30
#: C/restore-full.page:32
msgid "Click <gui style=\"button\">Forward</gui>."
msgstr "Nhấn nút <gui style=\"button\">Tiếp theo</gui>."

#. (itstool) path: item/p
#: C/restore-full.page:31
msgid ""
"Choose where to restore. Since this is a full system backup, leave it as the "
"default (which restores over your current install)."
msgstr ""
"Chọn nơi để phục hồi. Nếu sao lưu toàn hệ thống, hãy để mặc định (phục hồi "
"lên cài đặt hiện tại)."

#. (itstool) path: item/p
#: C/restore-full.page:33
msgid "Review your selections and click <gui style=\"button\">Restore</gui>."
msgstr ""
"Xem lại lựa chọn của bạn và nhấn nút <gui style=\"button\">Phục hồi</gui>."

#. (itstool) path: item/p
#: C/restore-full.page:34
msgid "Wait. It may take a long time to finish restoring."
msgstr "Hãy chờ đợi. Cần thời gian để phục hồi."

#. (itstool) path: item/p
#: C/restore-full.page:35
msgid ""
"Finally, reinstall any programs that you enjoyed in your previous install."
msgstr ""
"Cuối cùng, hãy cài lại bất kì chương trình nào bạn thích từ lần cài đặt "
"trước."

#. (itstool) path: note/p
#: C/restore-full.page:37
msgid ""
"If for whatever reason you have a problem after trying to restore with the "
"<gui style=\"checkbox\">Restore files to original locations</gui> option, "
"try restoring instead to a temporary directory and moving your files to "
"where they belong."
msgstr ""
"Nếu vì lí do nào đó mà bạn không sử dụng được tùy chọn <gui "
"style=\"checkbox\">Phục hồi các tập tin về vị trí ban đầu </gui>, hãy thử "
"phục hồi vào một thư mục khác và chép các tập tin về vị trí ban đầu."

#. (itstool) path: note/p
#: C/restore-full.page:38
msgid ""
"If that still doesn’t work, there are <link xref=\"restore-worst-case\">more "
"drastic things</link> you can try."
msgstr ""
"Nếu vẫn không có tác dụng, hãy còn những cách <link xref=\"restore-worst-"
"case\">mạnh tay hơn</link> bạn có thể thử."

#. (itstool) path: info/desc
#: C/restore-revert.page:13
msgid "Restore a previous version of a file"
msgstr "Phục hồi phiên bản trước của một tập tin"

#. (itstool) path: page/title
#: C/restore-revert.page:16
msgid "Restoring Specific Files"
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:18
msgid "Switch to the <gui>Restore</gui> tab."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:19
msgid "Wait for the storage location to be scanned."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:20
msgid "Browse to the folder containing the file you want to restore."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:21
msgid "Select the file or files you want to restore by clicking on them."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:22
msgid "Click <gui style=\"button\">Restore</gui> on the bottom left."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:23
msgid ""
"A restore dialog will appear. Choose whether you want to override the "
"original backed up files or restore to a new location."
msgstr ""

#. (itstool) path: item/p
#: C/restore-revert.page:24
msgid ""
"Click <gui style=\"button\">Restore</gui> and wait for the restore to finish."
msgstr ""

#. (itstool) path: note/p
#: C/restore-revert.page:26
msgid ""
"You can also choose the backup date from which you are restoring in the "
"bottom right of the <gui>Restore</gui> tab."
msgstr ""

#. (itstool) path: note/p
#: C/restore-revert.page:27
msgid "You can also revert files on the command line:"
msgstr ""

#. (itstool) path: note/screen
#: C/restore-revert.page:28
#, no-wrap
msgid "<_:span-1/>FILE1 FILE2"
msgstr "<_:span-1/>T.TIN1 T.TIN2"

#. (itstool) path: info/desc
#: C/restore-worst-case.page:13
msgid "What to do if you can’t restore your files"
msgstr "Làm gì nếu bạn không thể phục hồi các tập tin"

#. (itstool) path: page/title
#: C/restore-worst-case.page:17
msgid "When Everything Goes Wrong"
msgstr "Khi mọi thứ trở nên xấu đi"

#. (itstool) path: page/p
#: C/restore-worst-case.page:19
msgid ""
"<app>Déjà Dup</app> may fail. Maybe it crashes or gives you an error when "
"you try to restore. When you really need your data back, the last thing you "
"want do deal with is a bug. Consider filing a <link "
"href=\"https://launchpad.net/deja-dup/+filebug\">bug report</link> but in "
"the meantime, here are some approaches to workaround a misbehaving <app>Déjà "
"Dup</app> and get your data back."
msgstr ""
"<app>Déjà Dup</app> có thể đã gặp lỗi. Có lẽ nó bị ngắt hoặc báo lỗi đến bạn "
"khi bạn cố gắng để phục hồi. Khi bạn cần lấy lại dữ liệu, bạn phải giải "
"quyết lỗi phát sinh. Hãy xem qua <link href=\"https://launchpad.net/deja-"
"dup/+filebug\">báo cáo lỗi</link>, trong thời gian chờ đợi, đây là một số "
"cách tiếp cận cách giải quyết các vấn đề trục trặc của <app>Déjà Dup</app> "
"và cứu lại dữ liệu của bạn."

#. (itstool) path: note/p
#: C/restore-worst-case.page:21
msgid ""
"This is going to get technical. If none of this makes sense to you, don’t be "
"afraid to <link xref=\"support\">ask for help</link>."
msgstr ""
"Hỗ trợ kĩ thuật cho bạn. Nếu bạn vẫn còn thắc mắc, đừng ngần ngại <link "
"xref=\"support\">đặt câu hỏi</link>."

#. (itstool) path: item/p
#: C/restore-worst-case.page:24
msgid ""
"Open a <app>Terminal</app> window by pressing "
"<keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>."
msgstr ""
"Mở <app>Cửa sổ dòng lệnh</app> bằng cách nhấn giữ các phím "
"<keyseq><key>Ctrl</key><key>Alt</key><key>T</key></keyseq>."

#. (itstool) path: item/p
#: C/restore-worst-case.page:25
msgid ""
"Create the directory in which you will place your restored files. This guide "
"will use <_:file-1/>:"
msgstr ""
"Tạo thư mục chứa các tập tin được phục hồi. Hướng dẫn này sẽ sử dụng thư mục "
"<_:file-1/>:"

#. (itstool) path: section/title
#: C/restore-worst-case.page:30
msgid "Restoring with Duplicity"
msgstr "Phục hồi với Duplicity"

#. (itstool) path: section/p
#: C/restore-worst-case.page:32
msgid ""
"On the assumption that <app>Déjà Dup</app> is just not working for you right "
"now, you’re going to use the command line tool <app>duplicity</app> that is "
"used by <app>Déjà Dup</app> behind the scenes to back up and restore your "
"files."
msgstr ""
"Trường hợp <app>Déjà Dup</app> không hoạt động, bạn sẽ dùng công cụ dòng "
"lệnh <app>duplicity</app> vốn được <app>Déjà Dup</app> sử dụng để sao lưu và "
"phục hồi tập tin."

#. (itstool) path: note/p
#: C/restore-worst-case.page:34
msgid ""
"If you want more information about <app>duplicity</app> than presented here, "
"run <cmd>man duplicity</cmd>."
msgstr ""
"Nếu bạn muốn biết thêm nữa về <app>duplicity</app>, chạy lệnh <cmd>man "
"duplicity</cmd>."

#. (itstool) path: section/p
#: C/restore-worst-case.page:36
msgid ""
"The first thing we’ll try is a simple restore of all your data. Assuming "
"your files are on an external drive mounted as <_:file-1/> and you chose to "
"encrypt the backup, try this:"
msgstr ""
"Việc đầu tiên chúng ta thử đơn giản chỉ là phục hồi tất cả dữ liệu của bạn. "
"Nếu các tập tin nằm trên ổ đĩa gắn ngoài được gắn kết vào <_:file-1/> và bạn "
"chọn mã hóa bản sao lưu, hãy thử:"

#. (itstool) path: section/p
#: C/restore-worst-case.page:39
msgid "If you didn’t encrypt the backup, add <_:cmd-1/> to the command."
msgstr "Nếu bạn không mã hóa bản sao lưu, thêm <_:cmd-1/> vào lệnh."

#. (itstool) path: section/title
#: C/restore-worst-case.page:42
msgid "Other Backup Locations"
msgstr "Vị trí lưu bản sao lưu khác"

#. (itstool) path: section/p
#: C/restore-worst-case.page:43
msgid ""
"If you backed up to a remote or cloud server, the syntax you use with "
"<app>duplicity</app> will be different than the external drive example "
"above. See below for how to connect to your chosen backup location."
msgstr ""
"Nếu bạn sao lưu sang một địa chỉ từ xa hoặc dịch vụ đám mây, thì cú pháp "
"dùng trong <app>duplicity</app> sẽ khác với ổ đĩa gắn ngoài ở ví dụ trên. "
"Xem mục bên dưới để biết cách kết nối đến vị trí lưu bản sao lưu bạn đã chọn."

#. (itstool) path: note/p
#: C/restore-worst-case.page:44
msgid ""
"Remember to add <_:cmd-1/> to any example commands if your backup is not "
"encrypted."
msgstr ""
"Nhớ thêm <_:cmd-1/> vào bất kì lệnh ví dụ nào nếu bản sao lưu chưa được mã "
"hóa."

#. (itstool) path: section/p
#: C/restore-worst-case.page:45
msgid ""
"If <app>duplicity</app> appears to be having trouble connecting to your "
"server, try downloading all the <app>duplicity</app> files yourself to a "
"local folder and following the simpler example above."
msgstr ""

#. (itstool) path: item/title
#: C/restore-worst-case.page:48
msgid "FTP"
msgstr "FTP"

#. (itstool) path: item/p
#: C/restore-worst-case.page:49
#: C/restore-worst-case.page:58
#: C/restore-worst-case.page:66
msgid ""
"Look up your server address, port, username, and password and replace "
"instances of <var>SERVER</var>, <var>PORT</var>, <var>USERNAME</var>, and "
"<var>PASSWORD</var> in the example below with those respective values."
msgstr ""
"Tra cứu địa chỉ máy chủ, cổng, tên người dùng, và mật khẩu để thay thế vào "
"các mục tương ứng <var>MÁY CHỦ</var>, <var>CỔNG</var>, <var>TÊN NGƯỜI "
"DÙNG</var>, và <var>MẬT KHẨU</var> trong ví dụ bên dưới."

#. (itstool) path: item/p
#: C/restore-worst-case.page:50
#: C/restore-worst-case.page:59
#: C/restore-worst-case.page:67
#: C/restore-worst-case.page:76
msgid ""
"You may have specified a folder in which to put the backup files. In the "
"example below, replace any instance of <var>FOLDER</var> with that path."
msgstr ""

#. (itstool) path: item/p
#: C/restore-worst-case.page:51
msgid ""
"If you chose to not log in with a username, use <_:var-1/> as your "
"<var>USERNAME</var> below."
msgstr ""
"Nếu bạn chọn đăng nhập không dùng tên người dùng, giá trị <var>TÊN NGƯỜI "
"DÙNG</var> là <_:var-1/>."

#. (itstool) path: screen/var
#: C/restore-worst-case.page:53
#: C/restore-worst-case.page:54
#: C/restore-worst-case.page:61
#: C/restore-worst-case.page:62
#: C/restore-worst-case.page:70
#: C/restore-worst-case.page:71
#: C/restore-worst-case.page:79
#: C/restore-worst-case.page:80
#, no-wrap
msgid "USERNAME"
msgstr "TÊN NGƯỜI DÙNG"

#. (itstool) path: screen/var
#: C/restore-worst-case.page:53
#: C/restore-worst-case.page:54
#: C/restore-worst-case.page:61
#: C/restore-worst-case.page:62
#: C/restore-worst-case.page:70
#: C/restore-worst-case.page:71
#: C/restore-worst-case.page:79
#: C/restore-worst-case.page:80
#, no-wrap
msgid "SERVER"
msgstr "MÁY CHỦ"

#. (itstool) path: screen/var
#: C/restore-worst-case.page:53
#: C/restore-worst-case.page:54
#: C/restore-worst-case.page:61
#: C/restore-worst-case.page:62
#: C/restore-worst-case.page:70
#: C/restore-worst-case.page:71
#, no-wrap
msgid "PORT"
msgstr "CỔNG"

#. (itstool) path: screen/var
#: C/restore-worst-case.page:53
#: C/restore-worst-case.page:54
#: C/restore-worst-case.page:61
#: C/restore-worst-case.page:62
#: C/restore-worst-case.page:70
#: C/restore-worst-case.page:71
#: C/restore-worst-case.page:79
#: C/restore-worst-case.page:80
#, no-wrap
msgid "FOLDER"
msgstr "THƯ MỤC"

#. (itstool) path: item/title
#: C/restore-worst-case.page:57
msgid "SSH"
msgstr "SSH"

#. (itstool) path: item/title
#: C/restore-worst-case.page:65
msgid "WebDAV"
msgstr "WebDAV"

#. (itstool) path: item/p
#: C/restore-worst-case.page:68
msgid ""
"If you chose to use a secure connection (HTTPS) when backing up, use <_:var-"
"1/> instead of <_:var-2/> in the example below."
msgstr ""
"Nếu bạn muốn sử dụng kết nối an toàn (HTTPS) khi sao lưu, thay thế <_:var-"
"2/> bằng <_:var-1/> trong ví dụ bên dưới."

#. (itstool) path: item/title
#: C/restore-worst-case.page:74
msgid "Windows Share"
msgstr "Windows Share"

#. (itstool) path: item/p
#: C/restore-worst-case.page:75
msgid ""
"Look up your server address, username, and password and replace instances of "
"<var>SERVER</var>, <var>USERNAME</var>, and <var>PASSWORD</var> in the "
"example below with those respective values."
msgstr ""
"Tra cứu địa chỉ máy chủ, tên người dùng, và mật khẩu để thay thế vào các mục "
"tương ứng <var>MÁY CHỦ</var>, <var>TÊN NGƯỜI DÙNG</var>, và <var>MẬT "
"KHẨU</var> trong ví dụ bên dưới."

#. (itstool) path: item/p
#: C/restore-worst-case.page:77
msgid ""
"If you specified a domain for your Windows server, add it to the beginning "
"of <var>USERNAME</var> with a semicolon between them. For example, "
"<var>domain;username</var>."
msgstr ""
"Nếu bạn chỉ định một tên miền cho dịch vụ Windows, thêm nó vào phía trước "
"<var>TÊN NGƯỜI DÙNG</var> và ngăn cách chúng bằng dấu chấm phẩy. Ví dụ, "
"<var>tên_miền;tên_người_dùng</var>."

#. (itstool) path: section/title
#: C/restore-worst-case.page:88
msgid "Restoring by Hand"
msgstr "Phục hồi thủ công"

#. (itstool) path: section/p
#: C/restore-worst-case.page:90
msgid ""
"If even <app>duplicity</app> isn’t working for you, there may be little "
"hope. The backup file format is complicated and not easily manipulated. But "
"if you’re desperate, it’s worth a try."
msgstr ""
"Thậm chí <app>duplicity</app> không hoạt động, hy vọng vẫn còn dù là nhỏ "
"nhoi. Tập tin sao lưu khá phức tạp và không dễ đọc được. Nhưng nếu bạn đang "
"tuyệt vọng, nó đáng để thử."

#. (itstool) path: section/p
#: C/restore-worst-case.page:92
msgid ""
"If you used a remote or cloud server to store your backup, first download "
"all the <app>duplicity</app> files and place them in a folder on your "
"computer. Then enter that folder in your terminal."
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:94
msgid ""
"<app>Duplicity</app> stores your data in small chunks called volumes. Some "
"volumes belong to the periodic ‘full’ or fresh backups and others to the "
"‘inc’ or incremental backups. Starting with a full backup set of volumes at "
"volume 1, you’ll need to restore files volume by volume."
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:96
msgid ""
"If you encrypted your backup, first you must decrypt the volume with <_:cmd-"
"1/>. Say you have <_:file-2/>:"
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:99
msgid "Or to do all at once (make sure you have plenty of space!):"
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:102
msgid ""
"Now you have either a <_:file-1/> or a <_:file-2/> volume (depending on "
"whether you had to decrypt it or not). Use <_:cmd-3/> on whichever one you "
"have to extract the individual patch files:"
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:105
msgid "Or again, to do all at once:"
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:108
msgid ""
"Now the patch files will be in <_:file-1/> and <_:file-2/> folders. Each "
"file that spanned multiple volumes will be in <_:file-3/>. Let’s say you "
"backed up <_:file-4/>:"
msgstr ""

#. (itstool) path: section/p
#: C/restore-worst-case.page:112
msgid ""
"To recover data from incremental backups, use <_:cmd-1/> to stitch the files "
"together. See <_:cmd-2/> for usage."
msgstr ""

#. (itstool) path: info/desc
#: C/support.page:13
msgid "Get more help"
msgstr "Nhận thêm trợ giúp"

#. (itstool) path: page/p
#: C/support.page:18
msgid ""
"If you have unanswered questions or a problem you don’t understand, please "
"<link href=\"https://answers.launchpad.net/deja-dup\">ask about it</link> it "
"in the support forum."
msgstr ""
"Nếu bạn có câu hỏi chưa được trả lời hay một vấn đề không lí giải được, hãy "
"<link href=\"https://answers.launchpad.net/deja-dup\">đặt câu hỏi</link> tại "
"diễn đàn hỗ trợ."

#. (itstool) path: page/p
#: C/support.page:20
msgid ""
"Keep in mind that the people answering your questions are volunteers and "
"that while <app>Déjà Dup</app> is intended to work well, it <link "
"xref=\"license\">comes with no warranty</link>. So please be polite and "
"patient."
msgstr ""
"Hãy nhớ rằng những người giải đáp thắc mắc của bạn là những tình nguyện viên "
"và mục tiêu của <app>Déjà Dup</app> là giúp giải quyết tốt công việc, nó "
"<link xref=\"license\">không kèm theo sự bảo đảm nào</link>. Vì vậy hãy lịch "
"sự và kiên nhẫn."

#~ msgid "Automatic Backups"
#~ msgstr "Tự động sao lưu"

#~ msgid "Start now"
#~ msgstr "Bắt đầu ngay"

#~ msgid ""
#~ "Turn on the <gui>Automatic backups</gui> option on the <gui>Overview</gui> "
#~ "page."
#~ msgstr ""
#~ "Bật tùy chỉnh <gui>Tự động sao lưu</gui> tại trang <gui>Tổng quan</gui>."

#~ msgid "Your First Backup"
#~ msgstr "Sao lưu đầu tiên của bạn"

#~ msgid ""
#~ "Review your <link xref=\"prefs\">backup settings</link>, to confirm they are "
#~ "what you want."
#~ msgstr ""
#~ "Xem lại <link xref=\"prefs\">thiết lập sao lưu</link> để xác nhận chúng là "
#~ "những gì bạn muốn."

#~ msgid ""
#~ "Wait. It may take a long time to backup, especially if you are backing up to "
#~ "a remote server. Just let it run in the background while you do something "
#~ "else."
#~ msgstr ""
#~ "Xin hãy chờ đợi. Có thể mất thời gian để sao lưu, nhất là khi bạn sao lưu "
#~ "vào một máy ở xa. Chỉ chạy ở chế độ nền khi bạn cần làm việc khác."

#~ msgid "Backup Help"
#~ msgstr "Trợ giúp sao lưu"

#~ msgid "About <app>Déjà Dup Backup Tool</app>"
#~ msgstr "Giới thiệu <app>Công cụ sao lưu Déjà Dup</app>"

#~ msgid "Settings"
#~ msgstr "Thiết lập"

#~ msgid "Local Folder"
#~ msgstr "Thư mục trên máy"

#~ msgid "Removable Media"
#~ msgstr "Thiết bị lưu trữ di động"

#~ msgid "Creative Commons Attribution-Share Alike 3.0 Unported License"
#~ msgstr "Giấy phép Creative Commons Attribution-Share Alike 3.0 Unported"

#~ msgid ""
#~ "This is not recommended, because if your hardware fails, you will lose both "
#~ "your original data and your backups."
#~ msgstr ""
#~ "Không khuyên dùng tùy chọn này, vì khi phần cứng có vấn đề, bạn sẽ mất cả dữ "
#~ "liệu gốc và các bản sao lưu."

#~ msgid ""
#~ "Next, you need to install <app>Déjà Dup</app>. Your distribution vendor may "
#~ "package it for you. Otherwise, download and compile it."
#~ msgstr ""
#~ "Tiếp theo, bạn cần cài đặt <app>Déjà Dup</app>. Bản phân phối của bạn có thể "
#~ "đã có gói tin được đóng gói sẵn. Nếu không, bạn có thể tải và biên dịch nó."

#~ msgid "Browse to the folder containing the file you lost."
#~ msgstr "Duyệt đến thư mục chứa tập tin bị mất."

#~ msgid "Cloud Services"
#~ msgstr "Dịch vụ đám mây"

#~ msgid ""
#~ "Click on the <gui style=\"button\">Restore…</gui> button on the "
#~ "<gui>Overview</gui> page."
#~ msgstr ""
#~ "Nhấn nút <gui style=\"button\">Phục hồi…</gui> trên trang <gui>Tổng "
#~ "quan</gui>."

#~ msgid "Remote Servers"
#~ msgstr "Máy chủ từ xa"

#~ msgid ""
#~ "Choose the type of remote server to which you want to connect. Then you can "
#~ "enter the server information. Alternatively, if you know your server’s URL, "
#~ "choose <gui>Custom Location</gui> and enter it."
#~ msgstr ""
#~ "Chọn loại máy chủ từ xa mà bạn muốn kết nối. Sau đó nhập thông tin của máy "
#~ "chủ. Nếu bạn biết địa chỉ URL của máy chủ, chọn <gui>Vị trí tùy chọn</gui> "
#~ "và nhập vào."

#~ msgid ""
#~ "Click <guiseq><gui style=\"menuitem\">File</gui><gui "
#~ "style=\"menuitem\">Restore Missing Files…</gui></guiseq>."
#~ msgstr ""
#~ "Nhấn <guiseq><gui style=\"menuitem\">Tập tin</gui><gui "
#~ "style=\"menuitem\">Phục hồi tập tin bị mất…</gui></guiseq>."

#~ msgid "Restoring a Lost File"
#~ msgstr "Phục hồi tập tin bị mất"

#~ msgid ""
#~ "You can restore lost files from the command line as well, if you remember "
#~ "what their filenames were:"
#~ msgstr ""
#~ "Bạn cũng có thể phục hồi các tập tin từ dòng lệnh, nếu bạn nhớ tên chúng:"

#~ msgid ""
#~ "When you see the file you want to restore appear, select it and click <gui "
#~ "style=\"button\">Forward</gui>."
#~ msgstr ""
#~ "Khi bạn thấy tập tin muốn phục hồi hiện ra, chọn nó và nhấn <gui "
#~ "style=\"button\">Tiếp theo</gui>."

#~ msgid ""
#~ "Review your selections and click <gui style=\"button\">Restore</gui>. Note "
#~ "that this will overwrite your current version of the file."
#~ msgstr ""
#~ "Xem lại các lựa chọn của bạn và nhấn <gui style=\"button\">Phục hồi</gui>. "
#~ "Lưu ý, hành động này sẽ ghi đè lên phiên bản hiện tại của tập tin."

#~ msgid ""
#~ "When the <gui>Restore</gui> dialog appears, choose the date from which to "
#~ "restore."
#~ msgstr ""
#~ "Khi hộp thoại <gui>Phục hồi</gui> xuất hiện, chọn ngày muốn phục hồi."

#~ msgid "Amazon S3"
#~ msgstr "Amazon S3"

#, no-wrap
#~ msgid "ID"
#~ msgstr "ID"

#~ msgid "Rackspace Cloud Files"
#~ msgstr "Rackspace Cloud Files"

#, no-wrap
#~ msgid "KEY"
#~ msgstr "KHÓA"

#~ msgid ""
#~ "If you only back up when you remember to do so, you’re not backing up often "
#~ "enough. Fortunately, it’s easy to set up automatic backups."
#~ msgstr ""
#~ "Nếu việc sao lưu chỉ được tiến hành khi bạn nhớ đến nó, bạn sẽ không sao lưu "
#~ "đầy đủ. May thay, việc thiết đặt sao lưu tự động rất dễ dàng."

#~ msgid "Adjust your backup settings"
#~ msgstr "Điều chỉnh các thiết lập sao lưu của bạn"

#~ msgid ""
#~ "When the <gui>Restore</gui> dialog appears, it will scan for files that are "
#~ "in the backup but no longer in the folder."
#~ msgstr ""
#~ "Khi hộp thoại <gui>Phục hồi</gui> xuất hiện, nó sẽ quét các tập tin trong "
#~ "bản sao lưu nhưng không có trong thư mục."

#~ msgid "Restore a file that is no longer present"
#~ msgstr "Phục hồi một tập tin không còn tồn tại"
